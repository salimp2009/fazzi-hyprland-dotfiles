<h1 align="center">.dots</h1>
<h4 align="center">by fxzzi.</h4>
<h4 align="center">based on the tokyonight color scheme.</h4>

<img src="assets/preview.png" alt="Rice Showcase" align="right" width="450">

- **Operating System:** [arch](https://archlinux.org/)
- **Compositor:** [Hyprland](https://github.com/hyprwm/Hyprland)
- **Terminal Emulator:** [Kitty](https://github.com/kovidgoyal/kitty)
- **Status Bar:** [waybar](https://github.com/Alexays/Waybar/)
- **App Launcher:** [rofi](https://github.com/lbonn/rofi)
- **Notification Daemon:** [dunst](https://github.com/dunst-project/dunst)
- **Shell:** [zsh](https://www.zsh.org/)
- **Shell Plugin Manager:** [zgenom](https://github.com/jandamm/zgenom)
- **Browser:** [Firefox](https://www.mozilla.org/en-GB/firefox/new/)

## ✔️ Installation

```
$ git clone https://gitlab.com/fazzi/dotfiles.git && cd dotfiles && ./install
```

This will create symlinks and overwrite files. It will also install all packages from needed.list. Please backup your current configs before installing!

You will then have to enter files containing configs specific to my setup and edit them. eg. dual monitor configs

## 🖥️ Wallpapers

[Wallpaper Folder](https://gitlab.com/fazzi/dotfiles/-/tree/main/walls "walls folder")
